import { BrowserModule }            from '@angular/platform-browser';
import { NgModule }                 from '@angular/core';
import { FormsModule }              from '@angular/forms';
import { HttpModule, JsonpModule }  from '@angular/http';

//import { MdCoreModule }             from '@angular/material';
import { BrowserAnimationsModule }  from '@angular/platform-browser/animations';
//import { Router }                 from '@angular/router';

import { DragScrollModule } from 'angular2-drag-scroll';
//import { VideoJSComponent } from './videojs/videojs.component'

import { AppComponent }       from './app.component';
import { ToolbarComponent }   from './toolbar/toolbar.component';
import { NavbarComponent }    from './navbar/navbar.component';

import { LosModule }          from './los/los.module';
import { AppRoutingModule }   from './app-routing.module';

import { NavbarService }      from "./navbar/navbar.service";
import { LoService }          from "./los/los.service";
import { AdjacentLOService }  from './toolbar/adjacent-lo.service';


@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    NavbarComponent
    //VideoJSComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    //MdCoreModule,
    BrowserAnimationsModule,
    DragScrollModule,
    LosModule,
    AppRoutingModule
  ],
  providers: [
    NavbarService,
    LoService,
    AdjacentLOService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
  // Diagnostic only: inspect router configuration
  /*constructor(router: Router) {
    console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
  }*/
}
