import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';

import { LoStimService }    from '../los/lo-stim.service';
import { LoStructure }      from '../los/los';
import { LoService }        from '../los/los.service';
import {VideoJSComponent} from '../videojs/videojs.component'


@Component({
  selector: 'stim-child',
  templateUrl: './lo-stim.component.html',
  styleUrls: [ './lo-stim.component.css' ],
  providers: [ LoStimService, LoService ]
})

export class StimComponent implements OnInit {

  triggerRichMedia: object = {};
  loContent: LoStructure;
  carousel: Boolean = false;
  showCarouselImg: Array<boolean> = [];
  clickCounter:number = 0;
  showMagnifiedImage: boolean = false;

  @ViewChild('stim') stim;

  constructor (
    private loStimService: LoStimService,
    private loService: LoService,
    private viewContainerRef: ViewContainerRef
  ) {  }

  ngOnInit(){
    this.getStimContent();
  }

  getStimContent(): void {

    this.loService.getLoContent().then(loContent => {

      this.loContent = loContent;

      this.viewContainerRef.createEmbeddedView(this.stim); /* this works fine without any log error */

      this.carousel = this.loStimService.getCarouselState(this.loContent.metadata.LOTemplate);

      this.triggerRichMedia = this.loStimService.getRichMedia(this.loContent);
      console.log('StimComponent::triggerRichMedia:', this.triggerRichMedia);

    })

  }

  imageCarousele(direction){
    console.log('imageCarousele::selected', direction);
    this.showCarouselImg = this.loStimService.getCarouselImages(direction);
  }

  onDoubleClick(){
    this.clickCounter ++;
    let timer = setTimeout(() => {if(this.clickCounter == 1){this.clickCounter = 0;}}, 300);
    if(this.clickCounter == 2){
      this.showMagnifiedImage = true;
      this.clickCounter = 0;
      clearTimeout(timer);
    }
    console.log('lo-stim::onDoubleClick::clickCounter:', this.clickCounter);
  }
}
