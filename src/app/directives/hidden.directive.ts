
import { Directive, ElementRef, Renderer } from '@angular/core';

// Directive decorator
@Directive({
  selector: 'myHidden',
  host: {
    '(click)': 'ShowLinkedImage(1)'
  }
})

// Directive class
export class HiddenDirective {
  constructor(el: ElementRef, renderer: Renderer) {
    // Use renderer to render the element with styles
    renderer.setElementStyle(el.nativeElement, 'display', 'none');
  }

  ShowLinkedImage(imgNum){
    console.log('ShowLinkedImage:', imgNum);
  }
}
