import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app works!';

  // array of video elements
  public videos = ['http://vjs.zencdn.net/v/oceans.mp4'];

  constructor() {}
}
