import { QPresenterA2Page } from './app.po';

describe('q-presenter-a2 App', () => {
  let page: QPresenterA2Page;

  beforeEach(() => {
    page = new QPresenterA2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
